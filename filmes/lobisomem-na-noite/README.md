---
description: >-
  Um grupo secreto de caçadores de monstros se reúne no Castelo Bloodstone após
  a morte de sua líder e se envolve em uma misteriosa e mortal competição por
  uma poderosa relíquia.
cover: >-
  ../../.gitbook/assets/g_disneyplusoriginals_werewolfbynight_805_01_d1fb4a57.jpeg
coverY: 0
layout: editorial
---

# Lobisomem na Noite (2022)

## Trailer

{% embed url="https://www.youtube.com/watch?v=8uzyWaH68XY" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
