---
description: >-
  Em um mundo futuro distópico, um jovem lidera um bando de lutadores contra uma
  praga de gigantes empenhados em destruir o que resta da humanidade.
cover: ../../.gitbook/assets/Sasha-with-bow-and-arrow.webp
coverY: 0
layout: editorial
---

# Attack on Titan: Crimson Bow and Arrow (2014)

## Trailer

{% embed url="https://www.youtube.com/watch?v=O131paxxEVI" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
