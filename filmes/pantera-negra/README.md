---
description: >-
  Conheça a história de T'Challa, príncipe do reino de Wakanda, que perde o seu
  pai e viaja para os Estados Unidos, onde tem contato com os Vingadores.
cover: ../../.gitbook/assets/pantera-negra-webdoor.jpg
coverY: 139
layout: editorial
---

# Pantera Negra (2018)

<details>

<summary>Favoritado pela equipe Rave <img src="../../.gitbook/assets/veri (1).svg" alt="" data-size="line"></summary>

Este conteúdo foi favoritado por um integrante da equipe Rave. [Saiba mais sobre este selo](../../ajuda.md#conteudo-favoritado-pela-equipe-rave).

</details>

## Sequência

{% embed url="https://www.rave.tk/filmes/pantera-negra-wakanda-para-sempre" %}

## Trailer

{% embed url="https://www.youtube.com/watch?v=Fn4tlUYJajA" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
