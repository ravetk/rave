---
description: >-
  Um grupo de exploradores faz uso de um “buraco-de-minhoca” para superar as
  limitações de uma viagem espacial humana e conquistar as grandes distâncias
  relacionadas a uma travessia pelas estrelas.
cover: ../../.gitbook/assets/size_960_16_9_interstellar-023.webp
coverY: 0
layout: editorial
---

# Interestelar (2014)

## Trailer

{% embed url="https://www.youtube.com/watch?v=mbbPSq63yuM" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
