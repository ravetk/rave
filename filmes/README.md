---
description: >-
  Confira o nosso catálogo de filmes e suas respectivas categorias disponíveis
  no Rave.
layout: editorial
---

# Filmes

## Adicionado recentemente

{% embed url="https://www.rave.tk/filmes/pantera-negra-wakanda-para-sempre" %}

{% embed url="https://www.rave.tk/filmes/pantera-negra" %}

## Categorias

{% tabs %}
{% tab title="Ficção Cientifica" %}
{% embed url="https://www.rave.tk/filmes/battle-royale" %}

{% embed url="https://www.rave.tk/filmes/blonde" %}

{% embed url="https://www.rave.tk/filmes/interestelar" %}

{% embed url="https://www.rave.tk/filmes/lobisomem-na-noite" %}

{% embed url="https://www.rave.tk/filmes/pantera-negra" %}

{% embed url="https://www.rave.tk/filmes/pantera-negra-wakanda-para-sempre" %}

{% embed url="https://www.rave.tk/filmes/spencer" %}

{% embed url="https://www.rave.tk/filmes/tudo-em-todo-o-lugar-ao-mesmo-tempo" %}

{% embed url="https://www.rave.tk/filmes/minions-2-a-origem-de-gru" %}
{% endtab %}

{% tab title="Drama / Romance" %}
{% embed url="https://www.rave.tk/filmes/battle-royale" %}

{% embed url="https://www.rave.tk/filmes/blonde" %}

{% embed url="http://www.rave.tk/filmes/cinquenta-tons-mais-escuros" %}

{% embed url="https://www.rave.tk/filmes/pantera-negra-wakanda-para-sempre" %}

{% embed url="https://www.rave.tk/filmes/spencer" %}
{% endtab %}

{% tab title="Terror / Suspense" %}
{% embed url="https://www.rave.tk/filmes/battle-royale" %}

{% embed url="https://www.rave.tk/filmes/corra" %}

{% embed url="https://www.rave.tk/filmes/o-menu" %}

{% embed url="https://www.rave.tk/filmes/pantera-negra-wakanda-para-sempre" %}
{% endtab %}

{% tab title="Anime" %}
{% embed url="https://www.rave.tk/filmes/attack-on-titan-crimson-bow-and-arrow" %}

{% embed url="https://www.rave.tk/filmes/jujutsu-kaisen-0" %}
{% endtab %}
{% endtabs %}

{% tabs %}
{% tab title="Comédia" %}
{% embed url="https://www.rave.tk/filmes/duff-voce-conhece-tem-ou-e" %}

{% embed url="https://www.rave.tk/filmes/o-menu" %}
{% endtab %}

{% tab title="Musical" %}
{% embed url="https://www.rave.tk/filmes/black-is-king" %}
{% endtab %}
{% endtabs %}
