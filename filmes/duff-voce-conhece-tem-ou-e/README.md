---
description: >-
  Bianca descobre que foi escolhida pelas amigas do colégio como uma D.U.F.F.,
  sigla para amiga feia que serve para elas parecerem mais bonitas. Revoltada,
  Bianca pede a um atleta popular da escola.
cover: ../../.gitbook/assets/word.jpg
coverY: 72.4009900990099
layout: editorial
---

# D.U.F.F - Você Conhece, Tem ou É (2015)

## Trailer

{% embed url="https://www.youtube.com/watch?v=HDvEqkeiM6U" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
