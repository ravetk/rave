---
description: >-
  Um jovem casal visita um restaurante exclusivo em uma ilha remota onde o
  aclamado chef prepara um delicioso menu e algumas surpresas chocantes.
cover: ../../.gitbook/assets/themenu-1.jpeg
coverY: 59
layout: editorial
---

# O Menu (2022)

<details>

<summary>Favoritado pela equipe Rave <img src="../../.gitbook/assets/veri (1).svg" alt="" data-size="line"></summary>

Este conteúdo foi favoritado por um integrante da equipe Rave. [Saiba mais sobre este selo](../../ajuda.md#conteudo-favoritado-pela-equipe-rave).

</details>

## Trailer

{% embed url="https://www.youtube.com/watch?v=lfbYsIIFYaw" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}

{% content-ref url="legendado.md" %}
[legendado.md](legendado.md)
{% endcontent-ref %}
