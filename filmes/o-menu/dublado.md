---
layout: editorial
---

# Dublado

{% hint style="info" %}
Caso o player não apareça, aguarde 10 segundos para que ele carregue. Encontrou algum problema? Vá até a [página de ajuda](../../ajuda.md).
{% endhint %}

{% embed url="https://drive.google.com/file/d/10fd5DkZEId__Uiyn0X4iGmHg_IC7OXuY/view?usp=share_link" %}
