---
layout: editorial
---

# Legendado

{% hint style="info" %}
Caso o player não apareça, aguarde 10 segundos para que ele carregue. Encontrou algum problema? Vá até a [página de ajuda](../../ajuda.md).
{% endhint %}

{% embed url="https://drive.google.com/file/d/1vuAgilK5NXVXWeT7mhlB7w6-bff3zTk3/view?usp=share_link" %}
