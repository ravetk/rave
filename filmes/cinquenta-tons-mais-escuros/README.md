---
description: >-
  Incomodada com os hábitos e atitudes de Christian Grey, Anastasia decide
  terminar o relacionamento e focar no desenvolvimento de sua carreira. O desejo
  fala mais alto e ela volta aos jogos sexuais.
cover: >-
  ../../.gitbook/assets/rs_1024x578-161216054135-1024.fifty-shades-darker-3.121616.webp
coverY: 91.24752475247524
layout: editorial
---

# Cinquenta Tons Mais Escuros (2017)

## Trailer

{% embed url="https://www.youtube.com/watch?v=vJ41tFyuaFs" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
