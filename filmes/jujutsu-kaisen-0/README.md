---
description: >-
  O jovem Yuta Okkotsu ganha o controle de um espírito extremamente poderoso,
  então um grupo de feiticeiros o matriculam na Tokyo Prefectural Jujutsu High
  School, para ajudá-lo a controlar esse poder.
cover: >-
  ../../.gitbook/assets/O-FIlme-Jujutsu-Kaisen-0-Ja-Arrecadou-mais-de-116-Bilhoes-de-Ienes-MANUAL-DO-OTAKU.jpg
coverY: 49.10891089108911
layout: editorial
---

# Jujutsu Kaisen 0 (2022)

## Trailer

{% embed url="https://www.youtube.com/watch?v=359noIH-wNo" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
