---
description: >-
  A trama de Spencer será focada em um único fim de semana na vida da Diana, em
  que ela passou o Natal ao lado da família real na propriedade de Sandringham,
  em Norfolk, Inglaterra.
cover: ../../.gitbook/assets/f608x342-16045_45768_0.jpg
coverY: 0
layout: editorial
---

# Spencer (2021)

## Trailer

{% embed url="https://www.youtube.com/watch?v=yS1oP3VaXQA" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
