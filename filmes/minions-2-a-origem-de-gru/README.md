---
description: >-
  Nos anos 1970, o jovem Gru tenta entrar para um time de supervilões, mas a
  entrevista é desastrosa e ele e seus minions acabam fugindo do grupo de
  mal-feitores.
cover: ../../.gitbook/assets/2503-ffp-00375.webp
coverY: 28.58910891089109
layout: editorial
---

# Minions 2: A Origem de Gru (2022)

## Trailer

{% embed url="https://www.youtube.com/watch?v=oLCeh_pqjes" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}

{% content-ref url="legendado.md" %}
[legendado.md](legendado.md)
{% endcontent-ref %}
