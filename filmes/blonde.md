---
description: >-
  Esta cinebiografia fictícia reimagina a conturbada vida privada da lendária
  Marilyn Monroe e discute e o preço que a atriz pagou pela fama.
cover: ../.gitbook/assets/ana_de_armas_em_blonde.jpg
coverY: 0
layout: editorial
---

# Blonde (2022)

## Trailer

{% embed url="https://www.youtube.com/watch?v=AUfiFWUSp7k" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="blonde/dublado.md" %}
[dublado.md](blonde/dublado.md)
{% endcontent-ref %}
