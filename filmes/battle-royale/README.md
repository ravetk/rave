---
description: >-
  De ano em ano, 42 estudantes adolescentes japoneses são capturados e levados a
  uma ilha deserta. Equipados com armas, são forçados a disputar um jogo de vida
  ou morte, no qual apenas um sairá vivo.
cover: ../../.gitbook/assets/batalha-real-capa.jpg
coverY: 0
layout: editorial
---

# Battle Royale (2000)

## Trailer

{% embed url="https://www.youtube.com/watch?v=oKCN6hzD5J4" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}

{% content-ref url="legendado.md" %}
[legendado.md](legendado.md)
{% endcontent-ref %}
