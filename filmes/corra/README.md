---
description: >-
  Chris é um jovem fotógrafo negro que está prestes a conhecer os pais de Rose,
  sua namorada caucasiana. Com o tempo, ele percebe que a família dela esconde
  algo muito perturbador.
cover: ../../.gitbook/assets/getout.jpg
coverY: 0
layout: editorial
---

# Corra! (2017)

## Trailer

{% embed url="https://www.youtube.com/watch?v=mDGV5UucTu8" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}
