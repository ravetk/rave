---
description: >-
  Black Is King é um filme musical e álbum visual de 2020, dirigido, escrito e
  com produção executiva da cantora americana Beyoncé.
cover: ../../.gitbook/assets/BLACK_IS_KING_BEYONCE-7.png
coverY: 46.57425742574258
layout: editorial
---

# Black is King (2020)

## Trailer

{% embed url="https://www.youtube.com/watch?v=ybqvTebbfE8" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="legendado.md" %}
[legendado.md](legendado.md)
{% endcontent-ref %}
