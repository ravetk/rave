---
description: >-
  Uma ruptura interdimensional bagunça a realidade e uma inesperada heroína
  precisa usar seus novos poderes para lutar contra os perigos bizarros do
  multiverso.
cover: ../../.gitbook/assets/13095629643046.webp
coverY: 0
layout: editorial
---

# Tudo em Todo o Lugar ao Mesmo Tempo (2022)

<details>

<summary>Favoritado pela equipe Rave <img src="../../.gitbook/assets/veri (1).svg" alt="" data-size="line"></summary>

Este conteúdo foi favoritado por um integrante da equipe Rave. [Saiba mais sobre este selo](../../ajuda.md#conteudo-favoritado-pela-equipe-rave).

</details>

## Trailer

{% embed url="https://www.youtube.com/watch?v=kULcXm9V7aY" %}

## Em qual player deseja assistir? <a href="#player" id="player"></a>

{% content-ref url="dublado.md" %}
[dublado.md](dublado.md)
{% endcontent-ref %}

{% content-ref url="legendado.md" %}
[legendado.md](legendado.md)
{% endcontent-ref %}
