---
description: >-
  Joel e Ellie, uma dupla que se conecta através da dificuldade do mundo em que
  vivem, são forçados a passar por circunstâncias brutais e enfrentar monstros
  impiedosos.
cover: ../../.gitbook/assets/the_last_of_us_hbo_max_dublagem.jpg
coverY: 0
layout: editorial
---

# The Last of Us (2023)

{% hint style="info" %}
Novos episódios todo **domingo**, fique ligado no nosso telegram: [t.me/ravetk](https://t.me/ravetk)
{% endhint %}

<details>

<summary>Favoritado pela equipe Rave <img src="../../.gitbook/assets/veri (1).svg" alt="" data-size="line"></summary>

Este conteúdo foi favoritado por um integrante da equipe Rave. [Saiba mais sobre este selo](../../ajuda.md#conteudo-favoritado-pela-equipe-rave).

</details>

## Trailer

{% embed url="https://www.youtube.com/watch?v=IpjRuuFEPMk" %}

## Qual temporada deseja assistir? <a href="#temporadas" id="temporadas"></a>

{% content-ref url="1-temporada/" %}
[1-temporada](1-temporada/)
{% endcontent-ref %}
