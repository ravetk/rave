---
layout: editorial
---

# Dublado

{% hint style="info" %}
Caso o player não apareça, aguarde 10 segundos para que ele carregue. Encontrou algum problema? Vá até a [página de ajuda](../../../../ajuda.md).
{% endhint %}

{% embed url="https://drive.google.com/file/d/1-2AVHX-5sP9svR1JW8wC4V2Tuf3RjBIH/view" %}
