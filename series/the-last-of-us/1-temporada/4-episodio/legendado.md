---
layout: editorial
---

# Legendado

{% hint style="info" %}
Caso o player não apareça, aguarde 10 segundos para que ele carregue. Encontrou algum problema? Vá até a [página de ajuda](../../../../ajuda.md).
{% endhint %}
