---
layout: editorial
---

# Legendado

{% hint style="info" %}
Caso o player não apareça, aguarde 10 segundos para que ele carregue. Encontrou algum problema? Vá até a [página de ajuda](../../../../ajuda.md).
{% endhint %}

O player legendado será adicionado em breve. Confira o nosso Telegram:

{% embed url="https://t.me/ravetk" %}
