---
layout: editorial
---

# 1ª Temporada

{% hint style="info" %}
Procure abaixo episódios da primeira temporada da série [The Last of Us](../).
{% endhint %}

{% content-ref url="1-episodio/" %}
[1-episodio](1-episodio/)
{% endcontent-ref %}

{% content-ref url="2-episodio/" %}
[2-episodio](2-episodio/)
{% endcontent-ref %}

{% content-ref url="3-episodio/" %}
[3-episodio](3-episodio/)
{% endcontent-ref %}

{% content-ref url="4-episodio/" %}
[4-episodio](4-episodio/)
{% endcontent-ref %}

{% content-ref url="5-episodio/" %}
[5-episodio](5-episodio/)
{% endcontent-ref %}

