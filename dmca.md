---
layout: editorial
---

# DMCA

## Aviso Legal

O Rave é uma plataforma absolutamente legal e contém apenas links apontando para outros sites de vídeos (Google Drive), nós não hospedamos nenhum arquivo de mídia (avi, mkv, mpg, mpeg, vob, wmv, flv, mp4, mov, m2v, divx, xvid, 3gp, webm, ogv, ogg) protegido por direitos autorais em nosso servidor, nós apenas fazemos uma busca pelos links através da própria internet e organizamos os vídeos em nossa página de forma facilitada para o usuário. Nós não nos responsabilizamos pelo uso, não garantimos a funcionalidade de arquivos disponibilizados através de links de terceiros e também não temos controle sobre as publicidades exibidas nesses links. Se você tiver algum problema legal, favor entre em contato com os sites responsáveis pela hospedagem dos vídeos e unido da comprovação legal de detentor dos direitos autorais sobre a obra hospedada solicite a remoção e ela será feita conforme manda a Lei dos Direitos Autorais do Milênio Digital (DMCA).&#x20;

Mesmo não tendo poder sobre os arquivos aqui divulgados, sempre nos comprometemos a responder e tentar resolver todas as solicitações legais de forma pacífica, basta entrar em contato através do nosso email: [dmca@rave.tk](mailto:dmca@rave.tk)
