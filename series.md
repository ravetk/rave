---
description: >-
  Confira o nosso catálogo de séries e suas respectivas categorias disponíveis
  no Rave.
layout: editorial
---

# Séries

## Adicionado recentemente

{% embed url="https://www.rave.tk/series/the-last-of-us" %}

## Categorias

{% tabs %}
{% tab title="Ficção Cientifica" %}
{% embed url="https://www.rave.tk/series/the-last-of-us" %}
{% endtab %}

{% tab title="Drama / Romance" %}
{% embed url="https://www.rave.tk/series/the-last-of-us" %}
{% endtab %}

{% tab title="Terror / Suspense" %}

{% endtab %}

{% tab title="Anime" %}

{% endtab %}
{% endtabs %}
