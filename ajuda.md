---
description: >-
  Não sabe utilizar o Rave ou está enfrentando problemas com a plataforma?
  Confira este guia de soluções e dúvidas.
layout: editorial
---

# Ajuda

{% hint style="info" %}
**Atenção:** o site ainda está em fase de testes, então é possível que você encontre vários bugs na plataforma.
{% endhint %}

<details>

<summary>Como procurar filmes e/ou séries</summary>

Procure por <img src=".gitbook/assets/Frame.svg" alt="" data-size="line"> **Search...** no topo do site e digite o nome do conteúdo desejado. Se não aparecer nenhuma recomendação, é porque o conteúdo ainda não foi adicionado ou foi removido. [Saiba como adicionar conteúdos na plataforma aqui](broken-reference).

<img src=".gitbook/assets/example.gif" alt="" data-size="original">

</details>

<details>

<summary>Estou com problemas para acessar o site</summary>

É recomendado que altere a URL para [www.rave.tk](https://www.rave.tk), em vez de [rave.tk](https://rave.tk), já que o domínio redireciona para o subdomínio. Também é possível acessar pela URL [rave.tk](https://rave.tk), porém terá que reiniciar a página várias vezes até que seja redirecionado.

Se o problema ainda persistir, me chame no Twitter e tentarei resolver seu problema: [@ryvvn](https://twitter.com/ryvvn)

</details>

<details>

<summary>Erro ao tentar reproduzir o vídeo</summary>

Quando várias pessoas reproduzem o vídeo, muito provavelmente será impossível assisti-lo por um determinado tempo. Se este for o caso, você poderá baixar o conteúdo, com ou sem este erro.

Em algumas situações, o conteúdo pode estar indisponível porque o conteúdo foi removido por direitos autorais. Se este for o caso, por favor me chame no Twitter para que eu possa atualizar o conteúdo: [@ryvvn](https://twitter.com/ryvvn)

</details>

<details>

<summary>Player não aparece</summary>

Na maioria dos casos, o player não aparece por causa de problemas com a conexão à internet, principalmente se você estiver usando dados móveis. Você pode tentar alterando a sua conexão à internet por uma melhor ou seguir estes passos:

1. Limpe o cache do seu navegador.
2. Reinicie o seu aparelho.
3. Altere a conexão à internet.

</details>

<details>

<summary>Conteúdo favoritado pela equipe Rave</summary>

Se você viu este selo <img src=".gitbook/assets/veri.svg" alt="" data-size="line"> em algum conteúdo do nosso site, significa que **um integrante da nossa equipe favoritou este conteúdo**. Isto ajuda a identificar séries e filmes recomendados pela nossa equipe, te garantindo uma melhor experiência no nosso site.

</details>
