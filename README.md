---
description: Rave é uma plataforma de filmes e séries 100% atualizada e gratuita.
layout: landing
---

# Página Inicial

{% hint style="info" %}
Alguns filmes estão sendo atualizados por estarem inválidos. Consulte a nossa [página de ajuda](ajuda.md) caso tenha alguma dúvida.
{% endhint %}

## Seja bem-vindo ao <img src=".gitbook/assets/Rave Logo White.svg" alt="" data-size="line"> <a href="#bem-vindo" id="bem-vindo"></a>

O Rave é uma plataforma gratuita de filmes e séries sem anúncios [mantida pela comunidade](./#contribua). Atualizamos os conteúdos diariamente para que você assista em alta qualidade e definição.

Pronto para assistir filmes e séries sem se preocupar com propagandas chatas? Confira o nosso catálogo acessando as páginas abaixo:

{% content-ref url="filmes/" %}
[filmes](filmes/)
{% endcontent-ref %}

{% content-ref url="series.md" %}
[series.md](series.md)
{% endcontent-ref %}

### Agora temos um blog! <a href="#blog" id="blog"></a>

Ficou interessado no site? Fique atualizado sobre a plataforma, críticas e notícias acessando o nosso blog:

{% embed url="https://blog.rave.tk/" %}

## Contribua

Diariamente, vários arquivos protegidos por direitos autorais no Google Drive são removidos, o que pode afetar até mesmo os conteúdos disponibilizados na nossa plataforma. Nossa equipe incorpora os conteúdos que são encontrados na internet, então nenhum arquivo protegido por direitos autorais é hospedado por nós.

Saiba abaixo como você pode ajudar a manter a plataforma gratuita, atualizada e livre de anúncios.

### Faça uma doação <a href="#doacao" id="doacao"></a>

Ao doar, você nos ajuda a trazer mais conteúdos e melhorar e adicionar funcionalidades ao site, assim não será preciso adicionar anúncios no site. Caso queira doar através de outro meio de pagamento, fale com [@ryvvn](https://twitter.com/ryvvn).

****<img src=".gitbook/assets/picpay-logo.svg" alt="" data-size="line"> **PicPay**: [picpay.me/pov](https://picpay.me/pov)

### Envie um conteúdo para nós

Está faltando algum episódio, temporada ou quer adicionar um filme no nosso site? Preencha o formulário abaixo e nos ajude (caso não carregue, [clique aqui](https://docs.google.com/forms/d/e/1FAIpQLSfIoy3Lgxro0ADU5uDN6Phwnpyl9lk2Ep3ZyFrQ3eQ-y-cZRg/viewform?usp=sf\_link)):

{% embed url="https://docs.google.com/forms/d/e/1FAIpQLSfIoy3Lgxro0ADU5uDN6Phwnpyl9lk2Ep3ZyFrQ3eQ-y-cZRg/viewform?usp=sf_link" %}
